/* @flow */
import React from 'react';
import { View } from 'react-sketchapp';
import Section from '../components/Section';
import Button from '../components/Button';

const buttonSpacing = {
  width: 100,
  marginBottom: 10,
  marginRight: 5
};
type P = {
    btnColors: any,
    colors: any,
    sizes: any
};
const Buttons = ({ btnColors, colors, sizes }: P) => (
    <Section title="Buttons">
        <View name="Container" style={{ width:600, flexWrap: 'wrap', flexDirection: 'row'}}>
            {Object.keys(btnColors).map(name => (
                <View name={name} style={{ ...buttonSpacing }}>
                    <Button label={name} txtColor={colors.White} bgColor={colors[name]} brdColor={colors[name]} size={sizes.Normal} />
                </View>
            ))}
            <View name="Default" style={{ ...buttonSpacing  }}>
                <Button label="Default" txtColor={colors.GrayDark} bgColor={colors.White} brdColor={colors.GrayLight} size={sizes.Normal} />
            </View>
            <View name="White" style={{ ...buttonSpacing  }}>
                <Button label="White" txtColor={colors.GrayDark} bgColor={colors.White} brdColor={colors.White} size={sizes.Normal} />
            </View>
            <View name="Light" style={{ ...buttonSpacing }}>
                <Button label="Light" txtColor={colors.GrayDark} bgColor={colors.GrayLightest} brdColor={colors.GrayLightest} size={sizes.Normal} />
            </View>
            <View name="Dark" style={{ ...buttonSpacing }}>
                <Button label="Dark" txtColor={colors.White} bgColor={colors.GrayDark} brdColor={colors.GrayDark} size={sizes.Normal} />
            </View>
            <View name="Black" style={{ ...buttonSpacing }}>
                <Button label="Black" txtColor={colors.White} bgColor={colors.Black} brdColor={colors.Black} size={sizes.Normal} />
            </View>
        </View>
    </Section>
);

export default Buttons;
