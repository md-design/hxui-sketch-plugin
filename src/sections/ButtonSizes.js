/* @flow */
import React from 'react';
import { View } from 'react-sketchapp';
import Section from '../components/Section';
import Button from '../components/Button';

const buttonSpacing = {
    width: 100,
    marginBottom: 10,
    marginRight: 5
};
type P = {
    colors: any,
    sizes: any
};
const ButtonSizes = ({ colors, sizes }: P) => (
    <Section title="Button Sizes">
        <View name="Container" style={{ width:600, flexWrap: 'wrap', flexDirection: 'row'}}>
            <View name="Small" style={{ ...buttonSpacing }}>
                <Button label="Small" txtColor={colors.GrayDark} bgColor={colors.White} brdColor={colors.GrayLight} size={sizes.Small} />
            </View>
            <View name="Normal" style={{ ...buttonSpacing }}>
                <Button label="Normal" txtColor={colors.GrayDark} bgColor={colors.White} brdColor={colors.GrayLight} size={sizes.Normal} />
            </View>
            <View name="Large" style={{ ...buttonSpacing }}>
                <Button label="Large" txtColor={colors.GrayDark} bgColor={colors.White} brdColor={colors.GrayLight} size={sizes.Large} />
            </View>
        </View>
    </Section>
);

export default ButtonSizes;
