/* @flow */
/* eslint-disable react/jsx-filename-extension, import/no-named-as-default-member */

import React from 'react';
import { render, TextStyles, View } from 'react-sketchapp';
import designSystem from './designSystem';
import type { DesignSystem } from './designSystem';

import Label from './components/Label';
import Typography from './sections/Typography';
import Buttons from './sections/Buttons';
import ButtonSizes from './sections/ButtonSizes';
import ColorPalette from './sections/ColorPalette';
import InputFields from './sections/InputFields';

const Document = ({ system }: { system: DesignSystem }) => (
  <View name="HxUI Pattern Library">
    <View name="Intro" style={{ width: 420, marginBottom: system.spacing.Medium * 4 }}>
      <Label>
          Any update to this plugin via sketch will be lost when the plugin gets updated.
      </Label>
    </View>

    <Typography fonts={system.fonts} />
    <ColorPalette colors={system.colors} />
    <Buttons btnColors={system.buttonColors} colors={system.colors} sizes={system.buttonSizes} />
    <ButtonSizes colors={system.colors} sizes={system.buttonSizes} />
    <InputFields colors={system.colors} sizes={system.buttonSizes} />
  </View>
);

export default (context: any) => {
  TextStyles.create(
    {
      context,
      clearExistingStyles: true,
    },
    designSystem.fonts,
  );

  render(<Document system={designSystem} />, context.document.currentPage());
}
