/* @flow */
import React from 'react';
import { Text } from 'react-primitives';
import { spacing, fonts } from '../designSystem';

type Props = {
    label: string,
    bgColor: string,
    brdColor: string,
    txtColor: string,
    size: any
};

const buttonStyle = {
    borderRadius: 2,
    boxSizing: 'border-box',
    cursor: 'pointer',
    fontFamily: fonts.Body.fontFamily,
    fontWeight: 'regular',
    padding: 1,
    textAlign: 'center',
    width: 100,
    borderStyle:'solid',
    borderWidth:1
};



const Button = ({ label, bgColor, brdColor, txtColor, size }: Props) => (

    <Text name={label} style={{ ...buttonStyle, color: txtColor.hex, backgroundColor: bgColor.hex, borderColor: brdColor.hex, fontSize: size.fontSize, padding: size.padding }}>
        {label}
    </Text>
);

export default Button;
