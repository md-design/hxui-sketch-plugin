import { colors, spacing, fonts } from '../../designSystem';

export default {
  formElement: {
    marginBottom: spacing.Medium,
  },
  label: {
    display: 'block',
    fontFamily: fonts.Body.fontFamily,
    marginBottom: 0,
    fontSize: fonts.Body.fontSize - 4,
    color: colors.GreenLight,
  },
  textbox: {
    boxSizing: 'border-box',
    borderBottomWidth: 2,
    borderStyle: 'solid',
    borderColor: colors.GreenLight,
    backgroundColor: colors.White,
    fontFamily: fonts.Body.fontFamily,
    fontSize: fonts.Body.fontSize,
    lineHeight: fonts.Body.lineHeight,
    paddingTop: spacing.Small - 3,
    paddingBottom: spacing.Small,
    width: 300,
  }
};
